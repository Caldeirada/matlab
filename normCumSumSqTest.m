%Normalized  Cumulative Sum of Squares Test Statistic
%w details coef, v scalling coef 
function [D, Daux] = normCumSumSqTest(w,v,g,h,alpha)
  N = length(w);
  L = length(h);
	
  for i = 1:size(w)(1)
    Lj = (2^i-1)*(L-1) + 1;
    Ntil(i) = N - Lj + 1;
	D(i) = soma(w(i,:),Lj,Ntil(i));
  end
  
  Daux = D.*((N/2)^0.5);
  
end

#computes the test statistic D for the Cumulative Sum Squares
#using the computation from an introduction to wavelets for finance and economics
function D = soma(w,Lj,N)
	p = sum(w.^2);
	#N = length(w);
	for i = 1:N-1
		P(i) = sum(w(1:i).^2)/p;
	end
	
	for k = 1:N-1
		%dplus(k) = (k - Lj )/(N-1) - P(k);
		%dminus(k) = P(k) - (k - Lj)/(N-1);
		dplus(k) = k/(N-1) - P(k);
		dminus(k) = P(k) - (k -1)/(N-1);
	end
	
	D = max(max(dplus) ,max(dminus)); 
end

%function c = critVal(alpha,Ntil)
%  #aprox = ;
%  while abs(alpha-aprox) > 10^-6
	
%  end
%end

function val = func(x,l,Ntil)
  val = (-1)^l*exp(s*l^2 * Ntil * x^2);
end

function val = func1(x,l,Ntil)
  
end
