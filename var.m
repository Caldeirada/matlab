dephi = importdata('Detrend_PHI.dat');
detheta = importdata('Detrend_THETA.dat');
phi = importdata('LogNorm_PHI.dat');
theta = importdata('LogNorm_THETA.dat');
phi = phi(:,2);
theta = theta(:,2);

wv = 'sym4';
% phi = importdata('LogNorm_PHI.dat');
% theta = importdata('LogNorm_THETA.dat');

dephiWave = modwt(dephi,wv);
dethetaWave = modwt(detheta,wv);

dephiMra = modwtmra(dephiWave,wv);
dethetaMra = modwtmra(dethetaWave,wv);

phiWave = modwt(phi,wv);
thetaWave = modwt(theta,wv);

phiMra = modwtmra(phiWave,wv);
thetaMra = modwtmra(thetaWave,wv);

%plot para analisar variancia
figure;plot(phiMra(1,:))
figure;plot(phiMra(2,:))

%var
figure 1;modwtvar(phiWave)
figure 2;modwtvar(dephiWave)
[dephiVar1, dephiVar2] = modwtvar(dephiWave);
[phiVar1, phiVar2] = modwtvar(phiWave);

