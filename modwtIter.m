function [w,v] = modwtIter(X,g)
%w - low pass filter
%v - high pass filter
%X - serie; h,g - wavelet coeficent
w = zeros(length(X),1);
v = zeros(length(X),1);
n = length(g);
m = length(X);
for t = 1:1:m
    for l = 1:1:n
        ind = t-(l+1);
        if ind < 1 || ind > m
            ind = mod(ind,m);
            if ind == 0
                ind = m;
            end
        end
        w(t) = w(t) + h(l)*X(ind);
        v(t) = v(t) + g(l)*X(ind);
    end
end
end