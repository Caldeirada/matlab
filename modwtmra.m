
%computes the modwtmra of a given series, unlike modwt modwtmra isnt energy preserving
function [D, S] = modwtmra(w,v,g,h,j0)
  N = size(w)(1,1);
  n = length(v);
  D = zeros(j0,n);
  S = invmodwt(zeros(j0,n),v,g,h);
  
  for i = 1:1:j0
    d = invmodwt(w(i,:),zeros(1,n),g,h);
    for j = 1:1:(i-1)
      d = invmodwt(zeros(1,n),d,g,h);
    end
    D(i,:)=d;
  end	
end
