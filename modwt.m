  %G - coefficients for the dwt scalling filter
  %X - time series
  %j0 - wavelet level
  %compute the modwt of a given data series
  function [W,V] = modwt(X,g,h,j0)
    
    Jmax = floor(log2(length(X)));
    if j0 > Jmax
      j0 = Jmax;
    endif
    
    V = X;
    N = length(X);
    for j = 1:1:j0
      for t = 1:1:N
        k = t;
        hi = 0;
        gi = 0;
        for l = 1:1:length(h)
          hi = h(l)*V(k) + hi;
          gi = g(l)*V(k) + gi;
          k = mdi(k,j,N);
        end
        W(j,t) = hi;
        Vaux(t) = gi;
      end
      V = Vaux;
    end
  end
  
  %auxiliar function to compute the circular index
  function md = mdi(k,j,N)
    md = 0;
    md = k-(2^(j-1));%*(l-1);
    while md < 1
      md = md + N;
    end
  end
