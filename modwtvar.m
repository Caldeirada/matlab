#h o corresponde ao scaling filter
#variancia escala a escala e o intervalo de confiança
#errorbar(pontos,var,-var+confint(:,1)',-confint(:,2)'+var) para plot
#https://keisan.casio.com/exec/system/1180573197 importante
#errorbar(pontos,var,var-confintchi(:,1)',confintchi(:,2)'-var)
function [var, confInt, confIntchi] = modwtvar(w,v,g,h,conf)
	[k n] = size(w);
	L = length(h);
	#unbiased estimator for the wavelet variance
	for j = 1:k
		Lj(j) = ((2^j)-1)*(L-1) + 1;
		Ntilde(j) = n - Lj(j) + 1;
		var(j) = sum(w(j,(Lj(j)-1):(Ntilde(j)-1)).^2)/Ntilde(j);
	end
	Lj;
	Ntilde;
	
	confInt = zeros(k, 2);
	#confidence interval for the wavelet variance
	for j = 1:k
		gauss = stdnormal_inv(1-conf/2);
		
		A(j) = estimateA(var(j),n,Ntilde(j),w(j,:),Lj(j));
		
		confInt(j,:) = [(var(j)-gauss*(2*A(j)/Ntilde(j))^0.5) (var(j)+gauss*(2*A(j)/Ntilde(j))^0.5)];
	end
	
	confIntchi = zeros(k, 2);
	for j = 1:k
		eta = Ntilde(j)*(var(j)^2)/A(j)
		#eta = max(Ntilde(j)/(2^j),1)	
		
		confIntchi(j,:) = [(var(j)*eta/chi2inv(1-conf,eta)) (var(j)*eta/chi2inv(conf,eta))];
	end
end

function A = estimateA(var,n,nj,w,lj)
	s = estimateParseval(w,nj,lj,n);
	A = (var^4)/2 + s(1:nj - 1)*s(1:nj - 1)';
end

function s = estimateParseval(w,nj,lj,n)
	for i =1:1:nj - 1
		s(i) = (w(lj -1:n-1-i)*w(lj - 1 + i:n-1)')/nj;
	end
end
