%w - wavelet coefficients for the transform
%v - scaling coefficients
%H - wavelet coefficients
%compute the inverse modwt of a given data series
%can be used to compute the modwtmra
function [X] = invmodwt(w,v,g,h)
	L = length(h);
	[J, N] = size(w);
	X = v;
	for j = J:-1:1
		for t = 1:1:N
			k = t;
			xi = 0;
			for l = 1:1:L
				xi = h(l)*w(j,k) + g(l)*X(k) + xi;
				k = mdi(k,j,N);
			end
			Xaux(t) = xi;
		end
		X = Xaux;
	end
end

%auxiliar function to compute the circular index
function md = mdi(k,j,N)
	md = 0;
	md = k+(2^(j-1));%*(l-1);
	while md > N
		md = md - N;
	end
end
