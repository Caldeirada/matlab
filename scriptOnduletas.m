dephi = importdata('Detrend_PHI.dat');
detheta = importdata('Detrend_THETA.dat');
phi = importdata('LogNorm_PHI.dat');
theta = importdata('LogNorm_THETA.dat');
phi = phi(:,2);
theta = theta(:,2);

wv = 'coif4';
% phi = importdata('LogNorm_PHI.dat');
% theta = importdata('LogNorm_THETA.dat');

dephiWave = modwt(dephi,wv);
dethetaWave = modwt(detheta,wv);

dephiMra = modwtmra(dephiWave,wv);
dethetaMra = modwtmra(dethetaWave,wv);

phiWave = modwt(phi,wv);
thetaWave = modwt(theta,wv);

phiMra = modwtmra(phiWave,wv);
thetaMra = modwtmra(thetaWave,wv);


% subplot(8,1,1)
% plot(phi)
% for i = 1:1:8
%     subplot(8,1,i+1)
%     plot(phiMra(i,:))
% end
%    
% for i = 1:1:7
%     subplot(8,2,i)
%     plot(phiMra(7+i,:))
% end

enerPhi = sum(phi.^2);
enerTheta = sum(theta.^2);
for i = 1:1:size(phiWave,1)
    phiDec(i) = sum(phiWave(i,:).^2);
    thetaDec(i) = sum(thetaWave(i,:).^2);
end
enerDePhi = sum(dephi.^2);
enerDeTheta = sum(detheta.^2);
for i = 1:1:size(dephiWave,1)
    DephiDec(i) = sum(dephiWave(i,:).^2);
    DethetaDec(i) = sum(dethetaWave(i,:).^2);
end